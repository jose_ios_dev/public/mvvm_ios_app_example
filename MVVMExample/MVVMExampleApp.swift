//
//  MVVMExampleApp.swift
//  MVVMExample
//
//  Created by José Caballero on 16/03/24.
//

import SwiftUI

@main
struct MVVMExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
