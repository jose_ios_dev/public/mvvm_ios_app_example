//
//  UserViewModel.swift
//  MVVMExample
//
//  Created by José Caballero on 16/03/24.
//

import Foundation

class UserViewModel: ObservableObject {
    @Published private var user:UserModel?
    
    var userID:String{
        get{
            return self.user?.id ?? "No ID Defined"
        }
    }
    
    var username:String {
        get{
            return self.user?.name ?? "NoDefined"
        }
    }
    
    var userAge:String {
        get{
            return String(self.user?.age ?? 0)
        }
    }
    
    var isAdult:Bool{
        return self.user?.isAdult ?? false
    }
    
    init(user: UserModel) {
        self.user = user
    }
    
    /// Increment User Age
    func increaseAge() {
        guard let age = self.user?.age else {
            self.user?.age = 1
            return
        }
        self.user?.age = age < 130 ? age + 1 : age
    }
    
    /// Decrement User Age
    func decreaseAge() {
        guard let age = self.user?.age else {
            self.user?.age = 0
            return
        }
        
        self.user?.age = age > 0 ? age - 1 : 0
    }
    
    deinit {
        debugPrint("<<<< \(self) >>>>")
    }
}
