//
//  ContentView.swift
//  MVVMExample
//
//  Created by José Caballero on 16/03/24.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = UserViewModel(user: UserModel(id: Date().description, name: "McLOVIN", age: 17))
    
    var body: some View {
        UserDescriptionView(viewModel: self.viewModel)
    }
}

#Preview {
    ContentView()
}
