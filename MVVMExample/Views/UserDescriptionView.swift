//
//  UserDescriptionView.swift
//  MVVMExample
//
//  Created by José Caballero on 16/03/24.
//

import SwiftUI

struct UserDescriptionView: View {
    @ObservedObject var viewModel:UserViewModel
    
    var body: some View {
        VStack(spacing: 15) {
            
            Text("Detalles del Usuario").padding().font(.system(size: 24,weight: .bold))
            HStack(spacing: 8, content: {
                Text("ID:").font(.system(size: 20,weight: .semibold))
                Text(viewModel.userID)
                Spacer()
            })
            HStack(spacing: 8, content: {
                Text("Nombre:").font(.system(size: 20,weight: .semibold))
                Text(viewModel.username)
                Spacer()
            })
            HStack(spacing: 8, content: {
                Text("Edad:").font(.system(size: 20,weight: .semibold))
                Text(viewModel.userAge)
                Spacer()
            })
            HStack(spacing: 8, content: {
                Text("Mayoria de Edad:").font(.system(size: 20,weight: .semibold))
                Text(viewModel.isAdult ? "Usuario +18" : "Usuario < 18")
                Spacer()
            })
            HStack(spacing: 8, content: {
                Button(action: {
                    self.viewModel.decreaseAge()
                }, label: {
                    Text("Disminuir Edad").padding()
                }).border(.black, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/).background(Color.white).foregroundColor(.black)
                
                
                Button(action: {
                    self.viewModel.increaseAge()
                }){
                    Text("Aumentar Edad").padding()
                }.border(.black, width: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/).background(Color.white).foregroundColor(.black)
            }).padding()
        }.padding()
        Spacer()
    }
}

#Preview {
    UserDescriptionView(viewModel: UserViewModel(user: UserModel(id: Date().description, name: "McLOVIN-MVVM", age: 17)))
}
