//
//  UserModel.swift
//  MVVMExample
//
//  Created by José Caballero on 16/03/24.
//

import Foundation

struct UserModel {
    var id:String?
    var name:String?
    var age:Int?
    /// Define if user is 18+
    var isAdult:Bool {
        get{
            return self.age ?? 0 > 17
        }
    }
    
}
